
INTRODUCTION
------------

The YPlan module enables site administrators to add a block
showing affiliate ads from https://yplanapp.com/.

For more information, visit the project page:
https://www.drupal.org/sandbox/malcomio/2499271

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2499271

REQUIREMENTS
------------
This module has no dependencies on other contributed modules, but it
does require the core Block module to be enabled.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

   - Administer YPlan settings

     This allows users to control the configuration of the YPlan JavaScript.

* Configure YPlan settings such as affiliate ID
  in Administration » Configuration » Web Services » YPlan

* Add the YPlan affiliate widget block in a region of your choice
  at Administration  » Structure » Blocks
